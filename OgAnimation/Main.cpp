
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "core/CoreMinimal.h"
#include "Transform.h"
#include "render/Shader.h"


int main(void)
{
	GLFWwindow* window;

	Transform trans = Transform();

	std::cout << trans;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	if (!gladLoadGL())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	Shader* shader = new Shader();

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		/*glBegin(GL_TRIANGLES);
		glVertex2f(-1.0f,-1.0f);
		glVertex2f(0.0f, 0.2f);
		glVertex2f(0.5f, -0.5f);
		glEnd();*/


		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}