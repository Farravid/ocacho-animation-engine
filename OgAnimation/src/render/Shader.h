#pragma once

#include <iostream>
#include <map>
#include <string>
#include "core/Macros.h"

class Shader
{
	/*Reference to the OpenGL program*/
	PRIVATE(unsigned int, GET, m_handle, Handle);

private:
		std::map<std::string, unsigned int> m_attributes;
		std::map<std::string, unsigned int> m_uniforms;

	private:
		Shader(const Shader&);
		Shader& operator=(const Shader&);

	private:
		std::string readFile(const std::string& path);
		unsigned int compileVertexShader(const std::string& vertex);
		unsigned int compileFragmentShader(const std::string& fragment);
		bool linkShaders(unsigned int vertex, unsigned int fragment);

		void fillAttributes();
		void fillUniforms();

	public:
		Shader();
		Shader(const std::string& vertex, const std::string& fragment);
		~Shader();

		void load(const std::string& vertex, const std::string& fragment);

		void bind();
		void unBind();

		unsigned int getAttribute(const std::string& name);
		unsigned int getUniform(const std::string& name);

};