#pragma once


/*-----------------------------------------------------------------------------
Metadata macros
-----------------------------------------------------------------------------*/

#define GENERATE_NAME_VIRTUAL(...)\
	std::string getClassName() const override{\
		return std::string(# __VA_ARGS__);\
	};

#define GENERATE_CLASS_METADATA(...)\
	GENERATE_NAME_VIRTUAL(__VA_ARGS__);


/*-----------------------------------------------------------------------------
Definition macros 
-----------------------------------------------------------------------------*/

#define PRIVATE(type, accessor, name, faceName)\
    private:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define PROTECTED(type, accessor, name, faceName)\
    protected:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define GET(name, type, faceName)\
	const type get##faceName() const{ \
            return name;\
	};\

#define SET(name,type, faceName)\
	void set##faceName(type value){ \
	name = value;\
	};\

#define GET_SET(name, type, faceName)\
	const type get##faceName() const{ \
		return name;\
	};\
	void set##faceName(type value){ \
		name = value;\
	};\

#define NONE(name,type,faceName)
