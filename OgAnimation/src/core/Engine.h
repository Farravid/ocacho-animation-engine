#pragma once

#include <iostream>
#include "core/CoreMinimal.h"

class Engine : public BaseObject
{
private:
		Engine(const Engine&);
		Engine& operator=(const Engine&);
public:
		GENERATE_CLASS_METADATA(Engine);

		inline Engine() { }
		inline virtual ~Engine() { }
		inline virtual void init() { }
		inline virtual void update(float deltaSeconds) { }
		inline virtual void render(float aspectRadio) { }
		inline virtual void shutdown() { }

};