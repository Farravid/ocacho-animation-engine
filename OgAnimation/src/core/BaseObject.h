#pragma once

#include <iostream>

class BaseObject
{
	public:

		BaseObject() = default;

		//Destructores constructores ver que pasa
		
		virtual std::string getClassName() const { return "BaseObject"; }

		template<class T>
		bool isA() const { return dynamic_cast<const T*>(this) != nullptr; }

		template<class T>
		T* castTo() { return dynamic_cast<T*>(this); }

		//ids?

		//TODO: Create an actor with a transform and using the macros for getters and setters?

		//UGameplayStatics::Spawn<Tipo actor> molaria o algo no va aqui i know
};

