#include "Matrix4.h"
#include "core/CoreMinimal.h"
#include <cmath>

/*-----------------------------------------------------------------------------
Globals
-----------------------------------------------------------------------------*/

const Matrix4 Matrix4::identityMatrix(
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1);

/*-----------------------------------------------------------------------------
Macro helpers
-----------------------------------------------------------------------------*/

#define M4Dot(aRow, bCol) \
    a.v[0 * 4 + aRow] * b.v[bCol * 4 + 0] + \
    a.v[1 * 4 + aRow] * b.v[bCol * 4 + 1] + \
    a.v[2 * 4 + aRow] * b.v[bCol * 4 + 2] + \
    a.v[3 * 4 + aRow] * b.v[bCol * 4 + 3]


#define M4toV4Dot(mRow, x, y, z, w) \
    x * m.v[0 * 4 + mRow] + \
    y * m.v[1 * 4 + mRow] + \
    z * m.v[2 * 4 + mRow] + \
    w * m.v[3 * 4 + mRow]

#define M4SWAP(x,y)\
	{float t = x; x = y; y = t; }


#define M4_3X3MINOR(c0, c1, c2, r0, r1, r2) \
    (m.v[c0 * 4 + r0] * (m.v[c1 * 4 + r1] * m.v[c2 * 4 + r2] - m.v[c1 * 4 + r2] * m.v[c2 * 4 + r1]) - \
     m.v[c1 * 4 + r0] * (m.v[c0 * 4 + r1] * m.v[c2 * 4 + r2] - m.v[c0 * 4 + r2] * m.v[c2 * 4 + r1]) + \
     m.v[c2 * 4 + r0] * (m.v[c0 * 4 + r1] * m.v[c1 * 4 + r2] - m.v[c0 * 4 + r2] * m.v[c1 * 4 + r1]))


/*-----------------------------------------------------------------------------
Operators
-----------------------------------------------------------------------------*/

Matrix4 operator+(const Matrix4& a, const Matrix4& b)
{
	return Matrix4(
		a.xx + b.xx, a.xy + b.xy, a.xz + b.xz, a.xw + b.xw,
		a.yx + b.yx, a.yy + b.yy, a.yz + b.yz, a.yw + b.yw,
		a.zx + b.zx, a.zy + b.zy, a.zz + b.zz, a.zw + b.zw,
		a.wx + b.wx, a.wy + b.wy, a.wz + b.wz, a.ww + b.ww
	);
}

//---------------------------------------------------
//-----------------

Matrix4 operator*(const Matrix4& a, float v)
{
	return Matrix4(
		a.xx * v, a.xy * v, a.xz * v, a.xw * v,
		a.yx * v, a.yy * v, a.yz * v, a.yw * v,
		a.zx * v, a.zy * v, a.zz * v, a.zw * v,
		a.wx * v, a.wy * v, a.wz * v, a.ww * v
	);
}

//---------------------------------------------------
//-----------------

Matrix4 operator*(const Matrix4& a, const Matrix4& b)
{
	return Matrix4(
		OgMath::zero(M4Dot(0, 0)), OgMath::zero(M4Dot(1, 0)), OgMath::zero(M4Dot(2, 0)), OgMath::zero(M4Dot(3, 0)), // Column 0
		OgMath::zero(M4Dot(0, 1)), OgMath::zero(M4Dot(1, 1)), OgMath::zero(M4Dot(2, 1)), OgMath::zero(M4Dot(3, 1)), // Column 1
		OgMath::zero(M4Dot(0, 2)), OgMath::zero(M4Dot(1, 2)), OgMath::zero(M4Dot(2, 2)), OgMath::zero(M4Dot(3, 2)), // Column 2
		OgMath::zero(M4Dot(0, 3)), OgMath::zero(M4Dot(1, 3)), OgMath::zero(M4Dot(2, 3)), OgMath::zero(M4Dot(3, 3)) // Column 3
	);
}

//---------------------------------------------------
//-----------------

Vector4 operator*(const Matrix4& m ,const Vector4& v)
{
	return Vector4(
		M4toV4Dot(0, v.x, v.y, v.z, v.w),
		M4toV4Dot(1, v.x, v.y, v.z, v.w),
		M4toV4Dot(2, v.x, v.y, v.z, v.w),
		M4toV4Dot(3, v.x, v.y, v.z, v.w)
	);
}

//---------------------------------------------------
//-----------------

bool operator==(const Matrix4& a, const Matrix4& b)
{
	for(unsigned int i = 0; i < 16 ; i++)
	{
		if (a.v[i] - b.v[i] > EPSILON)
			return false;
	}
	return true;
}

//---------------------------------------------------
//-----------------

bool operator!=(const Matrix4& a, const Matrix4& b)
{
	return !(a == b);
}

//---------------------------------------------------
//-----------------

std::ostream& operator<<(std::ostream& os, const Matrix4& m)
{
	int _aux = 4;

	os << "Matrix4: \n";

	for (unsigned int i = 0; i < 16; i++)
	{
		os << "[" << m.v[i] << "]";

		_aux--;

		if (_aux == 0)
		{
			os << "\n";
			_aux = 4;
		}
	}

	return os;
}

/*-----------------------------------------------------------------------------
Methods
-----------------------------------------------------------------------------*/

Vector3 Matrix4::transformVector(const Matrix4& m, const Vector3& v)
{
	return Vector3(
		M4toV4Dot(0, v.x, v.y, v.z, 0.0f),
		M4toV4Dot(1, v.x, v.y, v.z, 0.0f),
		M4toV4Dot(2, v.x, v.y, v.z, 0.0f)
	);
}

//---------------------------------------------------
//-----------------

Vector3 Matrix4::transformPoint(const Matrix4& m, const Vector3& p)
{
	return Vector3(
		M4toV4Dot(0, p.x, p.y, p.z, 1.0f),
		M4toV4Dot(1, p.x, p.y, p.z, 1.0f),
		M4toV4Dot(2, p.x, p.y, p.z, 1.0f)
	);
}

//---------------------------------------------------
//-----------------

Vector3 Matrix4::transformPoint(const Matrix4& m, const Vector3& p, float& w)
{
	float _w = w;

	w = M4toV4Dot(3, p.x, p.y, p.z, _w);

	return Vector3(
		M4toV4Dot(0, p.x, p.y, p.z, _w),
		M4toV4Dot(1, p.x, p.y, p.z, _w),
		M4toV4Dot(2, p.x, p.y, p.z, _w)
	);
}

//---------------------------------------------------
//-----------------

void Matrix4::transpose()
{
	M4SWAP(yx, xy);
	M4SWAP(zx, xz);
	M4SWAP(wx, xw);
	M4SWAP(zy, yz);
	M4SWAP(wy, yw);
	M4SWAP(wz, zw);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::transposed(const Matrix4& m)
{
	return Matrix4(
		m.xx, m.yx, m.zx, m.wx,
		m.xy, m.yy, m.zy, m.wy,
		m.xz, m.yz, m.zz, m.wz,
		m.xw, m.yw, m.zw, m.ww
	);
}

//---------------------------------------------------
//-----------------

float Matrix4::determinant(const Matrix4& m)
{
	return  m.v[0] * M4_3X3MINOR(1, 2, 3, 1, 2, 3)
		- m.v[4] * M4_3X3MINOR(0, 2, 3, 1, 2, 3)
		+ m.v[8] * M4_3X3MINOR(0, 1, 3, 1, 2, 3)
		- m.v[12] * M4_3X3MINOR(0, 1, 2, 1, 2, 3);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::adjugate(const Matrix4& m)
{
	// Cofactor(M[i, j]) = Minor(M[i, j]] * pow(-1, i + j)
	Matrix4 cofactor;

	cofactor.v[0] = M4_3X3MINOR(1, 2, 3, 1, 2, 3);
	cofactor.v[1] = -M4_3X3MINOR(1, 2, 3, 0, 2, 3);
	cofactor.v[2] = M4_3X3MINOR(1, 2, 3, 0, 1, 3);
	cofactor.v[3] = -M4_3X3MINOR(1, 2, 3, 0, 1, 2);

	cofactor.v[4] = -M4_3X3MINOR(0, 2, 3, 1, 2, 3);
	cofactor.v[5] = M4_3X3MINOR(0, 2, 3, 0, 2, 3);
	cofactor.v[6] = -M4_3X3MINOR(0, 2, 3, 0, 1, 3);
	cofactor.v[7] = M4_3X3MINOR(0, 2, 3, 0, 1, 2);

	cofactor.v[8] = M4_3X3MINOR(0, 1, 3, 1, 2, 3);
	cofactor.v[9] = -M4_3X3MINOR(0, 1, 3, 0, 2, 3);
	cofactor.v[10] = M4_3X3MINOR(0, 1, 3, 0, 1, 3);
	cofactor.v[11] = -M4_3X3MINOR(0, 1, 3, 0, 1, 2);

	cofactor.v[12] = -M4_3X3MINOR(0, 1, 2, 1, 2, 3);
	cofactor.v[13] = M4_3X3MINOR(0, 1, 2, 0, 2, 3);
	cofactor.v[14] = -M4_3X3MINOR(0, 1, 2, 0, 1, 3);
	cofactor.v[15] = M4_3X3MINOR(0, 1, 2, 0, 1, 2);

	return transposed(cofactor);
}

//---------------------------------------------------
//-----------------

void Matrix4::invert()
{
	float det = determinant(*this);

	if (det < EPSILON) {
		std::cout << "WARNING: Matrix determinant is 0, you can't invert it \n";
		return;
	}

	*this = adjugate(*this) * (1.0f / det);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::inverse(const Matrix4& m)
{
	float det = determinant(m);

	if (det < EPSILON) {
		std::cout << "WARNING: Matrix determinant is 0, you can't invert it \n";
		return Matrix4();
	}

	return adjugate(m) * (1.0f / det);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::frustrum(float left, float right, float bottom, float top, float near, float far)
{
	if(left == right || top == bottom || near == far)
	{
		std::cout << "Invalid frustrum" << std::endl;
		return Matrix4();
	}

	return Matrix4(
		(2.0f * near) / (right -1), 0, 0, 0,
		0, (2.0f * near) / (top - bottom), 0, 0,
		(right + 1) / (right -1), (top + bottom) / (top + bottom), (-(far+near)) / (far - near), -1,
		0, 0, (-2.0f * far * near) / (far - near), 0
	);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::perspective(float fov, float aspectRadio, float nearDistance, float farDistance)
{
	float ymax = nearDistance * tanf(fov * PI / 360.0f);
	float xmax = ymax * aspectRadio;

	return frustrum(-xmax, xmax, -ymax, ymax, nearDistance, farDistance);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::orthographic(float left, float right, float bottom, float top, float near, float far)
{
	if (left == right || top == bottom || near == far)
	{
		std::cout << "Invalid orthographic" << std::endl;
		return Matrix4();
	}

	return Matrix4(
		2.0f / (right - 1), 0, 0, 0,
		0, 2.0f / (top - bottom), 0, 0,
		0, 0, -2.0f / (far - near), 0,
		-((right + 1) / (right - 1)), -((top + bottom) / (top + bottom)), -((far + near) / (far - near)), -1
	);
}

//---------------------------------------------------
//-----------------

Matrix4 Matrix4::lookAt(const Vector3& position, Vector3& target, const Vector3& p_up)
{
	Vector3 forward = Vector3::normalized(target - position) * -1.0f;
	Vector3 right = Vector3::crossProduct(p_up, forward);

	if (right == Vector3::zeroVector)
		return Matrix4();

	right.normalize();

	Vector3 up = Vector3::normalized(Vector3::crossProduct(forward, right));

	Vector3 t = Vector3
	(
		-Vector3::dotProduct(right, position),
		-Vector3::dotProduct(up, position),
		-Vector3::dotProduct(forward, position)
	);

	return Matrix4(
		right.x, up.x, forward.x, 0,
		right.y, up.y, forward.y, 0,
		right.z, up.z, forward.z, 0,
		t.x, t.y, t.z, 1
	);
}

