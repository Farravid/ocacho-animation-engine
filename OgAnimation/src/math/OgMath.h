#pragma once

#define PI 3.14159265359f
#define EPSILON 0.000001f
#define TO_DEGREES 57.2958f
#define TO_RADIANS 0.0174533f

class OgMath
{
	public:

		/*-----------------------------------------------------------------------------
		Methods
		-----------------------------------------------------------------------------*/

		/**
		* Returns 0 if v is nearly 0 or v if it's not
		*/
		static float zero(float v);

		/**
		* Returns true if v is nearly 0, false otherwise
		*/
		static bool isNearlyZero(float v);
};
