#include "Transform.h"

/*-----------------------------------------------------------------------------
Macro helpers
-----------------------------------------------------------------------------*/

#define SCALE_INVERSE(v)\
	fabs(v) < EPSILON ? 0.0f : 1.0f / v

/*-----------------------------------------------------------------------------
Operators
-----------------------------------------------------------------------------*/

Transform operator+(const Transform& a, const Transform& b)
{
	Transform out;

	out.scale = a.scale * b.scale;
	//Remeber the right-hand order, for this reason first b and later a, to execute first a
	out.rotation = b.rotation * a.rotation;

	//To calculate the position, first the scale, later the rotation and finally the position
	out.position = a.rotation * (a.scale * b.position);
	out.position = a.position + out.position;

	return out;
}

//---------------------------------------------------
//-----------------

std::ostream& operator<<(std::ostream& os, const Transform& t)
{
	os << "Transform: " << "\n" << "Position: " << t.position << "Rotation: " << t.rotation << "Scale:    " << t.scale;
	return os;
}

//---------------------------------------------------
//-----------------

Transform& Transform::operator=(const Transform& t)
{
	this->position = t.position;
	this->rotation = t.rotation;
	this->scale = t.scale;

	return *this;
}


/*-----------------------------------------------------------------------------
Methods
-----------------------------------------------------------------------------*/

Transform Transform::inverse(const Transform& t)
{
	Transform inv;

	inv.rotation = Quaternion::inverse(t.rotation);

	inv.scale.x = SCALE_INVERSE(t.scale.x);
	inv.scale.y = SCALE_INVERSE(t.scale.y);
	inv.scale.z = SCALE_INVERSE(t.scale.z);

	inv.position = inv.rotation * (inv.scale * -t.position);

	return inv;
}

//---------------------------------------------------
//-----------------

Transform Transform::mix(const Transform& a, const Transform& b, float t)
{
	return Transform(
		Vector3::lerp(a.position, b.position, t),
		Quaternion::nlerp(a.rotation,b.rotation,t),
		Vector3::lerp(a.scale, b.scale, t)
		);
}

//---------------------------------------------------
//-----------------

Matrix4 Transform::transformToMatrix4(const Transform& t)
{
	Vector3 right = t.rotation * Vector3::rightVector;
	Vector3 up = t.rotation * Vector3::upVector;
	Vector3 forward = t.rotation * Vector3::forwardVector;

	right = right * t.scale.x;
	up = up * t.scale.y;
	forward = forward * t.scale.z;

	Vector3 p = t.position;

	return Matrix4(
		right.x, right.y, right.z, 0,
		up.x, up.y, up.z, 0,
		forward.x, forward.y, forward.z, 0,
		p.x, p.y, p.z, 1
	);
}

//---------------------------------------------------
//-----------------

Transform Transform::matrix4ToTransform(const Matrix4& m)
{
	Transform out;

	out.position = Vector3(m.v[12], m.v[13], m.v[14]);
	out.rotation = Quaternion::matrix4ToQuaternion(m);

	Matrix4 rotScaleMat(
		m.v[0], m.v[1], m.v[2], 0,
		m.v[4], m.v[5], m.v[6], 0,
		m.v[8], m.v[9], m.v[10], 0,
		0, 0, 0, 1
	);

	Matrix4 invRotMat = Quaternion::quaternionToMatrix4(Quaternion::inverse(out.rotation));
	Matrix4 scaleSkewMat = rotScaleMat * invRotMat;

	out.scale = Vector3(
		scaleSkewMat.v[0],
		scaleSkewMat.v[5],
		scaleSkewMat.v[10]
	);

	return out;
}

//---------------------------------------------------
//-----------------

Vector3 Transform::transformPoint(const Transform& a, const Vector3& b)
{
	Vector3 out;

	out = a.rotation * (a.scale * b);
	out = a.position + out;

	return out;
}

//---------------------------------------------------
//-----------------

Vector3 Transform::transformVector(const Transform& a, const Vector3& b)
{
	Vector3 out;
	out = a.rotation * (a.scale * b);

	return out;
}