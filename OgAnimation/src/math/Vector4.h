#pragma once

template<typename T>
struct TVector4
{
	T x;
	T y;
	T z;
	T w;

	explicit inline TVector4() : x(T(0)), y(T(0)), z(T(0)), w(T(0)) { }
	explicit inline TVector4(T p_x, T p_y, T p_z, T p_w) : x(p_x), y(p_y), z(p_z), w(p_w) { }
	explicit inline TVector4(T* fv) : x(fv[0]), y(fv[1]), z(fv[2]), w(fv[3]) { }

};

typedef TVector4<float> Vector4;
typedef TVector4<int>	iVector4;
typedef TVector4<unsigned int> uiVector4;

