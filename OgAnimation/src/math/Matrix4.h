#pragma once

#include "core/CoreMinimal.h"

struct Matrix4
{
	/*-----------------------------------------------------------------------------
	Globals
	-----------------------------------------------------------------------------*/

	static const Matrix4 identityMatrix;

	/*-----------------------------------------------------------------------------
	Attributes and constructors

	Union to be able to access the data in different ways
	-----------------------------------------------------------------------------*/
	union
	{
		float v[16];

		struct
		{
			Vector4 right;
			Vector4 up;
			Vector4 forward;
			Vector4 position;
		};

		struct
		{
			float xx; float xy; float xz; float xw;
			float yx; float yy; float yz; float yw;
			float zx; float zy; float zz; float zw;
			float wx; float wy; float wz; float ww;
		};

		struct
		{
			float c0r0; float c0r1; float c0r2; float c0r3;
			float c1r0; float c1r1; float c1r2; float c1r3;
			float c2r0; float c2r1; float c2r2; float c2r3;
			float c3r0; float c3r1; float c3r2; float c3r3;
		};

		struct
		{
			float r0c0; float r1c0; float r2c0; float r3c0;
			float r0c1; float r1c1; float r2c1; float r3c1;
			float r0c2; float r1c2; float r2c2; float r3c2;
			float r0c3; float r1c3; float r2c3; float r3c3;
		};
	};

	/*Constructors*/
	explicit inline Matrix4() :
		xx(0.0f), xy(0.0f), xz(0.0f), xw(0.0f),
		yx(0.0f), yy(0.0f), yz(0.0f), yw(0.0f),
		zx(0.0f), zy(0.0f), zz(0.0f), zw(0.0f),
		wx(0.0f), wy(0.0f), wz(0.0f), ww(0.0f) { }

	explicit inline Matrix4(
		float p_xx, float p_xy, float p_xz, float p_xw,
		float p_yx, float p_yy, float p_yz, float p_yw,
		float p_zx, float p_zy, float p_zz, float p_zw,
		float p_wx, float p_wy, float p_wz, float p_ww) :
		xx(p_xx), xy(p_xy), xz(p_xz), xw(p_xw),
		yx(p_yx), yy(p_yy), yz(p_yz), yw(p_yw),
		zx(p_zx), zy(p_zy), zz(p_zz), zw(p_zw),
		wx(p_wx), wy(p_wy), wz(p_wz), ww(p_ww) { }

	explicit inline Matrix4(float* fv) :
		xx(fv[0]), xy(fv[1]), xz(fv[2]), xw(fv[3]),
		yx(fv[4]), yy(fv[5]), yz(fv[6]), yw(fv[7]),
		zx(fv[8]), zy(fv[9]), zz(fv[10]), zw(fv[11]),
		wx(fv[12]), wy(fv[13]), wz(fv[14]), ww(fv[15]) { }


	/*-----------------------------------------------------------------------------
	Methods
	-----------------------------------------------------------------------------*/

	/**
	* Change the orientation and scale of the v vector given the m Matrix4
	*/
	static Vector3 transformVector(const Matrix4& m, const Vector3& v);

	/**
	 * Translates the point in space given the m Matrix4
	 */
	static Vector3 transformPoint(const Matrix4& m, const Vector3& p);

	/**
	 * Translates the point in space given the m Matrix4
	 * 
	 * @param w: Holds the value for W, the four dimension
	 */
	static Vector3 transformPoint(const Matrix4& m, const Vector3& p, float& w);

	/**
	* Transpose this Matrix4
	*/
	void transpose();

	/**
	* Transpose the m Matrix4
	* 
	* @return The m matrix transposed
	*/
	static Matrix4 transposed(const Matrix4& m);

	/**
	* Calculate the determinant of the m matrix 
	*
	* @return The determinant of the m matrix
	*/
	static float determinant(const Matrix4& m);

	/**
	* Calculate the adjugate of the m matrix
	*
	* @return The determinant of the matrix m
	*/
	static Matrix4 adjugate(const Matrix4& m);

	/**
	* Invert this Matrix4
	*/
	void invert();

	/**
	* Invert the m Matrix4
	*
	* @return The m matrix inversed
	*/
	static Matrix4 inverse(const Matrix4& m);

	/**
	 * Create a frustrum Matrix4 given a left, right, bottom, top, near and far dimensions
	 */
	static Matrix4 frustrum(float left, float right, float bottom, float top, float near, float far);

	/**
	 * Create a perspective Matrix4 given a field of view, aspect radio, near distance, and far distance dimensions
	 */
	static Matrix4 perspective(float fov, float aspectRadio, float nearDistance, float farDistance);

	/**
	 * Create an orthographic Matrix4 given a left, right, bottom, top, near and far dimensions
	 */
	static Matrix4 orthographic(float left, float right, float bottom, float top, float near, float far);\

	/**
	* Generate the view matrix which is the inverse of the camera's transformation
	* Create a lookAt Matrix4 given a position, target and vector up reference.
	* 
	* @param target: The target we want to look at
	*/
	static Matrix4 lookAt(const Vector3& position, Vector3& target, const Vector3& up);
};

/*-----------------------------------------------------------------------------
Operators

Operators are NOT member functions to avoid left/right-hand problems
-----------------------------------------------------------------------------*/

Matrix4 operator+(const Matrix4& a, const Matrix4& b);
Matrix4 operator*(const Matrix4& a, float v);
Matrix4 operator*(const Matrix4& a, const Matrix4& b);
Vector4 operator*(const Matrix4& m, const Vector4& v);
bool operator==(const Matrix4& a, const Matrix4& b);
bool operator!=(const Matrix4& a, const Matrix4& b);
std::ostream& operator<<(std::ostream& os, const Matrix4& m);

